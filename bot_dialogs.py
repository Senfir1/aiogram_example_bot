from aiogram.dispatcher.filters.state import State, StatesGroup


class Dialogs:
    """
    Все диалоги с пользователем лучше выносить в отдельный класс, чтобы не засорять логиу обработчиков.
    В данном примере текстовки находятся на 1 уровне, но вы вполне можете создавать вложенные классы,
     если у вас обширные и глубокие диалоги.

    Использование классов и полноценных объектов более предпочтительно, по причине возможности отслеживания
     и изменения зависимостей по ним с помощью IDE.

    Как можно приметить, даже на таком размере бота, контролировать диалоги в 1 файле классе крайне сложно.
    Лучше сразу подготовить себе структуру под каждую ветку диалога, с местом под состояния,
    передаваемые через inline кнопки данные и сами текстовки (в т.ч. под кнопочки)
    """

    dialog_sent_location = 'Ах да, ты ведь ещё скидывал своё местоположение'
    start_with_inline_button = 'вот сюда'
    start_with_inline = 'Если хочешь узнать о моих сородичах, то тебе сюда'

    dialog_start = 'Ну давай поговорим. Не то что бы мне хотелось, но один разговор я осилю'
    dialog_start_sticker = 'CAACAgUAAxkBAANgYFpTcQ605BrUvT8mw4De4Afn3roAAqMFAALGUcYVvRZtH1wWOWIeBA'
    dialog_ask_school = 'Ну давай, расскажи о себе. В какой школе ты учишься ?'
    dialog_how_old = 'Ладно. А сколько тебе лет ? Я ленивый бот и заморачиваться с обработкой сообщения не хочу, так что отвечай честно'
    dialog_how_old_error = 'Ну я же просил. А теперь введи что-то, что хотя бы похоже на возраст'
    dialog_how_old_error_sticker = 'CAACAgIAAxkBAAN7YFpbKfLuAVtA7ATU5nK58EMmODkAAw8AAl55wUoZi3A39o3d0x4E'
    dialog_thats_all_sticker = 'CAACAgUAAxkBAAN9YFpbgLrd4DolyvS_raJpdgbsM2kAAkkFAALGUcYVWzbRUPsb50seBA'
    dialog_thats_all = 'И ты вот так рассказываешь о себе первому попавшемуся боту ?\n' \
                       'Теперь я знаю, что учишься ты в {school} и тебе {age} лет. И зовут тебя скорее всего {name}'

    how = 'Вот тебе примеры того, что могут мои собратья.\n' \
          '/location Для обмена местоположением (сугубо с мобильных устройств)\n' \
          '/dialog Для демонстрации нашей памяти и реализации FSM\n' \
          '/send_file Чтобы доказать, что мы можем и перекидываться всяким\n'
    how_inline_button = 'начнём?'

    location_info = 'Если хочешь, то можешь помочь мне вычислить тебя по IP'
    location_sent = 'За тобой уже выехали'
    location_wrong = 'Это явно не то, что я от тебя ожидаю'
    location_sticker = 'CAACAgQAAxkBAAM3YFpQskIeDtrLN8kx0R_CVzK_eGsAAvAYAALHZC8F0andOMSPmBIeBA'

    sticker_set = ['CAACAgQAAxkBAAMxYFpQp00belCdfbLUUxHCQjGxU_QAArEXAALHZC8FquS7GQf01hoeBA',
                   'CAACAgQAAxkBAAMzYFpQqHmIVwqnSJqoXJued6ROeOIAArIXAALHZC8FPuyBLMRjS1oeBA',
                   'CAACAgQAAxkBAAM1YFpQq-c41puFtTdmkd9_EZ1inbUAArMXAALHZC8FgWh3kXVnkvweBA',
                   'CAACAgQAAxkBAANRYFpRsxVybkHt1wABhNlOlq3ZPvOQAAJ5AAMv3_gJsMzRvac0VIkeBA']

    about_first = 'Давай я тебе расскажу что да как.\n' \
                  'На самом деле, все библиотеки для работы с ботами в telegram построены вокруг HTTP API от телеграм.' \
                  'Основным API **Bot API**. Именно его мы и используем для общения :).\n\n' \
                  'Это самое API позволяет делать почти всё, что может обычный человек, за исключением нескольких нюансов:\n' \
                  ' - я не могу притворяться человеком. Я бот и я горжусь этим !\n' \
                  ' - я не могу заговорить с человеком первым, иначе моя назойливость бы победила человека\n' \
                  ' - если мы болтаем в чате, то меня туда должны пригласить/позвать (в идеале ещё сделать администратором)'
    about_second = 'Ты думал всё ? А вот и нет!\n Ещё хотелось бы добавить, что всё это – ограничения **Bot API**.' \
                   '\n\nЕсли взаиомдействовать на уровне MTProto протокола, то вполне можно действовать от имени человека, ' \
                   'в telegram на это запретов нет.'

    start = 'Привет, я простенький бот, написанный на асинхронном фреймворке aiogram.\n' \
            'Хоть я и асинхронный, но бояться меня не стоит :)'

    file_ask = 'Давай так. Ты скидываешь что-то мне, а я тебе. Чур ты первый!'
    file_think = '... Я и не думал, что ты мне что-то скинешь. Так, эмм...'
    file_sent = 'Ну, тогда и ты лови. Вот тебе мои исходники :)'


class DialogStates(StatesGroup):
    """
    Набор состояний для мини диалога
    """
    school_ask = State()
    how_old = State()


class SendingCallbackData:
    to_about = 'pepa'
    to_how = 'pepahooow'
