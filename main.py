"""
Простенький пример по написанию ботов на aiogram.
Стоит указать, что данный формат написания **НЕ ПООЩРАЕТСЯ**

Данный пример создан исключительно для демонстрации **функционала**.
aiogram – крайне мощная библиотека, позволяющая удобно переиспользовать ваш код,
поэтому большую часть копипасты можно избежать.

Пожалуйста, помните об этом, дабы упростить жизнь себе и другим
"""

import asyncio
import logging
import random
from typing import Union

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext

import config

from aiogram import Bot, Dispatcher, executor, types
from aiojobs import create_scheduler

from bot_dialogs import Dialogs, DialogStates, SendingCallbackData
from utils import get_scheduler

logging.basicConfig(level=logging.INFO)

# понадобится нам для сохранения состояний и информации о пользователе
storage = MemoryStorage()

bot = Bot(token=config.API_TOKEN)
dp = Dispatcher(bot, storage=storage)


@dp.message_handler(commands=['start'], state='*')
async def start(message: types.Message, state: FSMContext):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    keyboard.add(types.KeyboardButton('/about О мне и моих братьях'),
                 types.KeyboardButton('/how Примеры использования'))

    await message.reply(Dialogs.start, reply_markup=keyboard)
    await asyncio.sleep(1)

    # создаём "встроенную" в сообщение клавиатуру
    inline_keyboard = types.InlineKeyboardMarkup()
    inline_keyboard.add(types.InlineKeyboardButton(
        Dialogs.start_with_inline_button,
        callback_data=SendingCallbackData.to_about))

    await message.answer(Dialogs.start_with_inline, reply_markup=inline_keyboard)

    if state:
        await state.finish()


@dp.message_handler(commands=['about'], state='*')
@dp.callback_query_handler(lambda data: data.data == SendingCallbackData.to_about)
async def about(message: types.Message):
    await bot.send_message(message.from_user.id, Dialogs.about_first, parse_mode='markdown',
                           reply_markup=types.ReplyKeyboardRemove())

    async def send_after():
        """
        Создаём задачу по отправке сообщения.

        Сначала мы в задаче подождём +-3 секунды, а уже потом отправим сообщение.

        **await** перед вызовом говорит нам о том, что мы дождёмся выполнения этой задачи
        (а ведь можем и не ждать)
        """
        inline_keyboard = types.InlineKeyboardMarkup()
        inline_keyboard.add(types.InlineKeyboardButton(
            Dialogs.how_inline_button,
            callback_data=SendingCallbackData.to_how))

        # На самом деле делать отдельную функцию не обязательно, т.к. обработчики ассинхронные,
        # то бот без проблем может обслуживать несколько запросов одновременно
        # это скорее демонстрация возможности отложенных задач
        await asyncio.sleep(3)
        await bot.send_message(message.from_user.id, Dialogs.about_second, parse_mode='markdown',
                               reply_markup=inline_keyboard)

    # планировщик это крайне удобный объект для отслеживания задач, которые мы не хотим ждать
    scheduler = get_scheduler(dp)
    # в данном случае, при указании скобок мы создаём корутину или иначе говоря вызываем асинхронную функцию
    # фактически, в этот же момент она начнём своё выполнение, но вот получить результат мы сразу не сможем
    # (а вот если укажем await перед вызовом, то получим именно результат)
    await scheduler.spawn(send_after())
    # т.к. нам не интересен результат выполнения нашей отправки, мы просто кидаем эту задачу в планировщик,
    # чтобы она выполнилась где-то там в фоне


# если нужно, то можно задать свою функцию обработки (я про lambda через строку)
# но лучше посмотреть на уже готовые вариации обработчиков
@dp.message_handler(commands=['how'], state='*')
@dp.callback_query_handler(lambda data: data.data == SendingCallbackData.to_how)
async def how(message: Union[types.Message, types.InlineQuery]):
    # если обработчик принимает и обычное и inline сообщение,
    # то нужно быть внимательным, т.к. у них отличаются поля

    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    keyboard.add(types.KeyboardButton('/location'))
    keyboard.add(types.KeyboardButton('/dialog'))
    keyboard.add(types.KeyboardButton('/send_file'))

    await bot.send_message(message.from_user.id, Dialogs.how, reply_markup=keyboard)


@dp.message_handler(state='*', content_types=types.ContentType.LOCATION)
@dp.message_handler(commands=['location'], state='*', content_types=types.ContentType.TEXT)
async def share_location(message: types.Message):
    # вот тут тоже советую быть внимательным и подметить, что это именно объект ContentType без `s` на конце
    if message.content_type == types.ContentType.TEXT:
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add(types.KeyboardButton('ну давай', request_location=True))
        await bot.send_message(message.from_user.id, Dialogs.location_info, reply_markup=keyboard)

    elif message.content_type == types.ContentType.LOCATION:
        await bot.send_message(message.from_user.id, Dialogs.location_sent,
                               reply_markup=types.ReplyKeyboardRemove())

        async def send_with_pending():
            await asyncio.sleep(1.5)
            await message.answer_sticker(Dialogs.location_sticker)

        await get_scheduler(dp).spawn(send_with_pending())
        # ну а пока в фоне отслается сообщеньице, мы запишем данные
        storage = await dp.storage.get_data(chat=message.chat.id, default={})
        storage['is_sent_location'] = True
        await dp.storage.update_data(chat=message.chat.id, data=storage)


@dp.message_handler(state='*', content_types=types.ContentType.STICKER)
async def message_with_sticker(message: types.Message):
    print(message.sticker.file_id)
    await message.answer_sticker(random.choice(Dialogs.sticker_set))


@dp.message_handler(commands=['dialog'], state='*')
async def start_dialog(message: types.Message):
    await message.answer(Dialogs.dialog_start, reply_markup=types.ReplyKeyboardRemove())
    # Чуть чуть драматургии
    # В целом, чем более "интересно" себя ведёт бот, тем больше им пользуются
    # Под интересностью "можете понимать всё что вам угодно, главное не переборщить
    await asyncio.sleep(0.5)
    await message.answer_sticker(Dialogs.dialog_start_sticker)

    # А вот тут и начинается самое интересное.
    # Мы задаём пользователю состояние, за которое мы теперь можем цепляться в обработчиках
    await DialogStates.school_ask.set()
    # когда и какое состояние будет у пользователя – решать нам.
    # Таким образом мы можем навигировать пользователей в системе без их ведома и даже без их действий
    # (мы ведь можем крутить задачки в фоне через планировщик `scheduler`)

    await asyncio.sleep(1)
    await message.answer(Dialogs.dialog_ask_school)


# и теперь мы цепляемся за состояние в обработчике
@dp.message_handler(state=DialogStates.school_ask)
async def ask_school(message: types.Message):
    # storage – хранилище данных (в нашем случае MemoryStorage)
    # там хранится информация о пользователе (если мы её туда сохраняем)
    # и о состоянии в котором находится пользователь
    storage = await dp.storage.get_data(chat=message.chat.id, default={})

    storage['school'] = message.text
    await message.answer(Dialogs.dialog_how_old)

    # не забываем сохранить информацию
    await dp.storage.update_data(chat=message.chat.id, data=storage)

    # ну и само собой сместить пользователя на новое состояние
    await DialogStates.how_old.set()


@dp.message_handler(state=DialogStates.how_old)
async def ask_age(message: types.Message, state: FSMContext):
    try:
        age = int(message.text)
        if age < 3 or age > 99:
            raise ValueError

        storage = await dp.storage.get_data(chat=message.chat.id, default={})
        storage['age'] = message.text
        await dp.storage.update_data(chat=message.chat.id, data=storage)

        text = Dialogs.dialog_thats_all.format(age=storage['age'], school=storage['school'],
                                               name=message.from_user.first_name or message.from_user.username)
        await message.answer(text)
        if storage.get('is_sent_location', False):
            async def send_after():
                await asyncio.sleep(2)
                await message.answer(Dialogs.dialog_sent_location)

            await get_scheduler(dp).spawn(send_after())

        if state:
            await state.finish()

    except ValueError:
        # для таких случаев лучше завезти отдельный обработчик
        # с ошибками и пробрасывать в информацию об ошибке всё, что вам нужно
        await message.answer_sticker(Dialogs.dialog_how_old_error_sticker)
        await asyncio.sleep(1)
        await message.answer(Dialogs.dialog_how_old_error)


# 2 обработчика, потому что 1 сугубо под файлы, а второй завязан на получамых данных в сообщении
@dp.message_handler(content_types=types.ContentType.DOCUMENT)
@dp.message_handler(commands='send_file', content_types=types.ContentType.TEXT)
async def handle_file(message: types.Message):
    # А вот и пример с файликом :)
    if message.content_type == types.ContentType.TEXT:
        await message.answer(Dialogs.file_ask)
    elif message.content_type == types.ContentType.DOCUMENT:
        # как я уже упоминал, обрачивать всё это в функцию и запускать через планировщик в данном случае
        # не обязательно. Но лично мне так спокойнее
        async def with_pending():
            await asyncio.sleep(2)
            await message.answer(Dialogs.file_think)
            await asyncio.sleep(3)
            await message.answer(Dialogs.file_sent)
            await asyncio.sleep(2)
            await message.answer_document(types.InputFile('./sources.zip'))

        await get_scheduler(dp).spawn(with_pending())


async def on_startup(dp: Dispatcher):
    logging.getLogger('startup').info("run startup functions")
    # при запуске приложения мы создаём планировщик
    # раньше его создать нельзя, ему нужно находится в асинхронном контексте/функции
    dp.storage.scheduler = await create_scheduler()


async def on_shutdown(dp: Dispatcher):
    logging.getLogger('shutdown').info("waiting for scheduler")
    scheduler = get_scheduler(dp)
    # как истинно порядочные люди завершаем работу планировщика
    await scheduler.close()


if __name__ == '__main__':
    logging.getLogger('startup').info("Starting via pooling")
    executor.start_polling(dp,
                           on_startup=on_startup,
                           on_shutdown=on_shutdown)
